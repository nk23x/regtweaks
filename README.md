Regtweaks (Win 7 - Win 10) Collection files original created under the GNUv2 license 2015 by [CHEF-KOCH](https://github.com/CHEF-KOCH). Some tweaks are taken from the www, some are found by myself.


**Current Status: Over 600+ tweaks**


Project Goal
------------

* All of the tweaks could be integrated into image creations tools (vLite, WinToolKit,..) to simplify things, especially if you like to re-install Windows from the ground without need to apply each tweak one by one.
* Show hidden things that aren't visible via GUI under Windows. Sometimes it's easier/faster to do this via .reg instead of search for the toggles.
* Fix everything under ToDo.



Any problems, questions or something wrong?
------------

* Feel free to open an [issue ticket](https://github.com/CHEF-KOCH/regtweaks/issues) and I will look at it asap. - Pull requests or ideas are always welcome!




Win 7-10 Home Users Notice
------------

The Home Edition doesn't support the GPEDIT.MSC stuff, so here is how to add them back [here's the link how](http://drudger.deviantart.com/art/Add-GPEDIT-msc-215792914) or [this](http://www.askvg.com/how-to-enable-group-policy-editor-gpedit-msc-in-windows-7-home-premium-home-basic-and-starter-editions/).



Project History
------------

- [x] 01.09.2015	All tweaks are now in english.
- [x] 25.08.2015	Added some image creation tools which are able handle the registry tweaks.
- [x] 18.08.2015	Initial upload and first release.



ToDo
------------

- [ ] Add new tweaks (high-prio)
- [ ] Sort all tweaks maybe via .html or .js parsser to easier access them with e.g. a search function (like RSW?) (low-prio)
- [ ] Delete dubs. (high-prio)
- [ ] Remove not working ones of course this needs some testers (high-prio)
- [x] Rename them to english (some are in ger./ru.) (high-prio)
- [ ] Fix [reported problems](https://github.com/CHEF-KOCH/regtweaks/issues) (high-prio)
- [ ] Add on/off toggles (registry/batch files) to revert all changes in case something goes wrong - s$it happens! 



Tools
------------

* [RSAT for Windows 10](https://www.microsoft.com/en-us/download/details.aspx?id=45520)
* [NTLite](https://www.ntlite.com) / [vLite](http://www.vlite.net/)
* [WinToolkit](https://www.Wincert.net)



Useful stuff
------------

* [Windows 10 findings and tips | Reddit](https://www.reddit.com/r/Windows10/comments/3f48kd/windows_10_tips_and_tricks/)
* [fix windows 10 | isleaked](https://fix10.isleaked.com/)
